#!/bin/bash
# This script stubs a typical file structure found in invenio repositories 
#  on travis CI to help testing the migration helper script

mkdir testrepo
cd testrepo

# Step 2
touch .travis.yml

# Step 3
echo "# Matches the exact files either package.json or .travis.yml" > .editorconfig
echo "[{package.json,.travis.yml}]" >> .editorconfig

# Step 4.1
echo ".. image:: https://img.shields.io/travis/inveniosoftware/invenio-oauthclient.svg" > README.rst
echo "        :target: https://travis-ci.org/inveniosoftware/invenio-oauthclient" >> README.rst

# Step 4.2
echo "https://travis-ci.com/inveniosoftware/invenio-oauthclient/pull_requests" > CONTRIBUTING.rst

# Step 5
echo "python -m check_manifest --ignore \".travis-*\" && \
python -m sphinx.cmd.build -qnNW docs docs/_build/html && \
docker-services-cli up ${DB}
python -m pytest
tests_exit_code=\$?
docker-services-cli down
exit \"\$tests_exit_code\"" > run-tests.sh

echo "Created stub structure"